/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libio
 * FILE NAME: manage_io.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libio Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_io.c
 * @brief
 * @author      Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include "time_series.h"
#include "IO.h"
#include "ext_var_glo_io.h"

/**
 * @brief Creates the buffer for the file stored in various reading piles
 * @return Newly created s_out_io* pointer
 */
s_file_io *IO_create_file() {
  s_file_io *pfile;

  pfile = new_file_io();
  bzero((char *)pfile, sizeof(s_file_io));

  return pfile;
}

/**
 * @brief Creates and initializes the structure used to parse a file
 * @return Newly created s_out_io* file-type pointer
 */
s_out_io *IO_create_output() {
  s_out_io *pout;

  pout = new_out_io();
  bzero((char *)pout, sizeof(s_out_io));
  pout->nb_output = CODE_IO;
  pout->format = UNFORMATTED_IO; // NF 19/04/2018 sets the output type to UNFORMATTED by default
  pout->spatial_scale = CODE_IO; // NG 20/07/2020
  pout->write_out = YES_IO;      // NG 31/07/2020

  return pout;
}

/**
 * @brief Creates the structure used to parse a file
 * @return
 */
s_in_io *IO_create_input() {
  s_in_io *pin;
  int i;

  pin = new_in_io();
  bzero((char *)pin, sizeof(s_in_io));

  for (i = 0; i < NPILE_IO; i++)
    pin->current_read_files[i] = IO_create_file();

  pin->yyin = yyin;

  return pin;
}

/**
 * @brief Procedure used to display errors automatically called in the case of syntax errors
 * Specifies the file name and the line where the error was found.
 * @return
 */
void IO_yyerror(char *s, s_in_io *pin, FILE *fp) {
  if (pin->pile >= 0) {
    LP_error(fp, "File %s, line %d : %s\n", pin->current_read_files[pin->pile]->name, pin->line_nb + 1, s);
  }
}

/**
 * @brief TBD
 * @return TBD
 */
void IO_init_time(s_out_io *pout, double t_init, double simul_length, double deltat) {
  int j;

  pout->deltat = deltat;

  for (j = 0; j < NTIME_IO; j++) {
    pout->t_out[j] = t_init + (pout->deltat * simul_length) * ((j - 1) * j) / 2; // BL juste pour avoir t[OUTPUT][INI]=t[CALC][INI]=t[OUTPUT][CUR]=t[CALC][CUR]=t_init (END = 2)
  }
}

/**
 * @brief Writes data from an array to a FILE* stream.
 */
void IO_write_val(double *val, int nb_val, FILE *fp) { fwrite(val, sizeof(double), nb_val, fp); }

/**
 * @brief Writes binary record header to a FILE* stream.
 */
void IO_write_header(int *header, int nb_header, FILE *fp) { fwrite(header, sizeof(int), nb_header, fp); }

/**
 * @brief Refreshes time t_out attribute associated with a s_out_io* structure
 */
void IO_refresh_t_out(s_out_io *pout) {
  pout->t_out[INI_IO] = pout->t_out[CUR_IO];
  pout->t_out[CUR_IO] += pout->deltat;
}

/**
 * @brief Creates and initializes a s_inout_set_io* pointer
 * @return Newly created and initialized s_inout_set_io* pointer
 */
s_inout_set_io *IO_create_inout_set(int noutput) {
  int i;
  s_inout_set_io *pinout;

  pinout = new_inout_set();
  bzero((char *)pinout, sizeof(s_inout_set_io));
  pinout->calc = (int *)malloc(noutput * sizeof(int));

  for (i = 0; i < NIFLU; i++) {
    pinout->init_from_file[i] = NO_TS;
  }
  return pinout;
}

/**
 * @brief TBD
 * @return TBD
 */
s_out_io *IO_init_out_struct(double t_ini, double t_end, double dt) {
  s_out_io *pout;

  pout = new_out_io();
  bzero((char *)pout, sizeof(s_out_io));
  pout->t_out[INI_IO] = t_ini;
  pout->t_out[FINAL_IO] = t_end;
  pout->t_out[CUR_IO] = t_ini;
  pout->deltat = dt;
  pout->type_out = ALL_IO;
  pout->format = UNFORMATTED_IO; // NF 19/4/2018 sets the output type to UNFORMATTED by default
  pout->final_state = NO_IO;
  pout->init_from_file = NO_IO;
  pout->spatial_scale = CODE_IO; // NG : 20/07/2020
  pout->write_out = YES_IO;      // NG : 31/07/2020

  return pout;
}

/**
 * @brief TBD
 * @return TBD
 */
void IO_init_out_file(s_out_io *pout, int out, int date1, int date2, FILE *fp) {
  int j;
  FILE *fout;
  char *name;
  char *name_mod;
  char *name_out;
  char *name_file;
  char *cmd;
  int errno;

  name_out = IO_name_out(out);
  name_mod = IO_name_mod(out);

  cmd = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));

  // SW 14/03/2022 system(cmd) may have some problems, replace it by IO_mkdir
  sprintf(cmd, "%s/Output_%s", getenv("RESULT"), name_mod);
  IO_mkdir(cmd);
  free(cmd);

  if (pout->format == FORMATTED_IO) {

    if (pout->name == NULL) {
      name = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + strlen(name_out) + ALLOCSCD_LP) * sizeof(char));
      sprintf(name, "%s/Output_%s/%s", getenv("RESULT"), name_mod, name_out);
      pout->name = name;
    }
    name_file = (char *)malloc((strlen(pout->name) + ALLOCSCD_LP) * sizeof(char));
    sprintf(name_file, "%s.%d%d.txt", pout->name, date1, date2);
    LP_printf(fp, " opening file %s\n", name_file);
    pout->fout = fopen(name_file, "w+");

    if (pout->fout == NULL) {
      LP_error(fp, "cannot open the file %s\n", name_file);
    }
  } else {
    if (pout->name == NULL) {
      name = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + strlen(name_out) + ALLOCSCD_LP) * sizeof(char));
      sprintf(name, "%s/Output_%s/%s", getenv("RESULT"), name_mod, name_out);
      pout->name = name;
    }

    name_file = (char *)malloc((strlen(pout->name) + ALLOCSCD_LP) * sizeof(char));
    sprintf(name_file, "%s.%d%d.bin", pout->name, date1, date2);
    LP_printf(fp, " opening file %s\n", name_file);
    pout->fout = fopen(name_file, "wb");

    if (pout->fout == NULL) {
      LP_error(fp, "cannot open the file %s\n", name_file);
    }
  }

  free(name_mod);
  free(name_out);
  free(name_file);
}

/**
 * @brief Creates and opens a final state type file
 */
void IO_init_final_state_file(s_out_io *pout, int out, FILE *fp) {
  int j;
  FILE *fout;
  char *name;
  char *name_mod;
  char *name_file;
  char *cmd;
  int errno;

  name_mod = IO_name_mod(out);
  cmd = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));

  // SW 14/03/2022 system(cmd) may have some problems, replace it by IO_mkdir
  sprintf(cmd, "%s/FinState_%s", getenv("RESULT"), name_mod);
  IO_mkdir(cmd);
  free(cmd);
  name = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/FinState_%s/%s", getenv("RESULT"), name_mod, name_mod);
  name_file = (char *)malloc((strlen(name) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name_file, "%s.FinState.bin", name);
  LP_printf(fp, " opening file %s\n", name_file);
  pout->fout = fopen(name_file, "wb");

  if (pout->fout == NULL) {
    LP_error(fp, "cannot open the file %s\n", name_file);
  }
  free(name_mod);
  free(name_file);
  free(name);
}

/**
 * @brief Writes a data block (record) to a binary file (including headers markers)
 */
void IO_print_bloc_bin(double **val, int nele_tot, int output, FILE *fp) {
  int i;

  for (i = 0; i < output; i++) {
    IO_write_header(&nele_tot, 1, fp);
    IO_write_val(val[i], nele_tot, fp);
    IO_write_header(&nele_tot, 1, fp);
  }
  val = IO_free_val(val, output);
}

/**
 * @brief TBD
 * @return TBD
 */
void *IO_freed(void *val) {
  free(val);
  return NULL;
}

/**
 * @brief TBD
 * @return TBD
 */
double **IO_free_val(double **val, int output) {
  int i;

  for (i = 0; i < output; i++)
    val[i] = IO_freed(val[i]);

  return NULL;
}

/**
 * @brief TBD
 * @return TBD
 */
void IO_link_mb(s_out_io *pout, int out, int year1, int year2, FILE *flog) {
  char *cmd;

  if (pout->format == UNFORMATTED_IO) {
    cmd = (char *)malloc((2 * strlen(pout->name) + ALLOCSCD_LP) * sizeof(char));
    sprintf(cmd, "ln -s %s.%d%d.bin %s.bin", pout->name, year1, year2, pout->name);
    system(cmd);
    free(cmd);
  }
}

/**
 * @brief TBD
 * @return TBD
 */
FILE *IO_return_file(char **name_out_folder, char *name_file, int npile, FILE *fp) {
  int i, out = 0;
  char *name;
  FILE *fpin = NULL;

  for (i = 0; i < npile; i++) {
    if (name_out_folder[i] != NULL) {
      name = (char *)malloc((strlen(name_out_folder[i]) + strlen(name_file) + ALLOCSCD_LP) * sizeof(char));
      sprintf(name, "%s/%s", name_out_folder[i], name_file);
      fpin = fopen(name, "rb");
      free(name);
      name = NULL;
      if (fpin != NULL) {
        out = 1;
        break;
      }
    } else {
      break;
    }
  }

  if (out == 0) {
    LP_error(fp, "file %s can't be opened\n", name_file);
  }
  free(name_file);

  return fpin;
}

/**
 * @brief Create nested directory using mkdir of C function
 * @internal SW 14/03/2022
 */
void IO_mkdir(const char *dir) {
  char tmp[STRING_LENGTH_LP];
  char *p = NULL;
  size_t len;

  snprintf(tmp, sizeof(tmp), "%s", dir);
  len = strlen(tmp);
  if (tmp[len - 1] == '/') {
    tmp[len - 1] = 0;
  }
  for (p = tmp + 1; *p; p++)
    if (*p == '/') {
      *p = 0;
      mkdir(tmp, S_IRWXU);
      *p = '/';
    }
  mkdir(tmp, S_IRWXU);
}