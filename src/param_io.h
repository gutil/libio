/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libio
 * FILE NAME: param_io.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libio Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        param_io.h
 * @brief       IO library parameters
 * @author      Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

/**
 * @def YYERROR_VERBOSE
 * @brief Parameter for error messages with LEX and YACC
 */
#define YYERROR_VERBOSE 1

/**
 * @def YYMAXDEPTH
 * @brief Maximum number of lines that can be read in a file
 */
#define YYMAXDEPTH 500000

/**
 * @def NPILE_IO
 * @brief Maximum number of input files which can be read with include()
 */
#define NPILE_IO 25

/**
 * @def CODE_IO
 * @brief Default error code
 */
#define CODE_IO -9999

/**
 * @def VERSION_IO
 * @brief Library version ID
 */
#define VERSION_IO 0.19

/**
 * @def DELTAT_IO
 * @brief
 */
#define DELTAT_IO 86400

/**
 * @def MIDDLE_IO
 * @brief
 */
#define MIDDLE_IO NEXTREMA_IO

/**
 * @enum beginning_end_io
 * @brief Browsing direction for pointer chains
 */
enum beginning_end_io { BEGINNING_IO, END_IO, NEXTREMA_IO };

/**
 * @enum answer_io
 * @brief Boolean answer-type values
 */
enum answer_io { YES_IO, NO_IO, NANSWER_IO };

/**
 * @enum format_io
 * @brief IO File formats
 */
enum format_io { FORMATTED_IO, UNFORMATTED_IO, NFORMAT_IO };

/**
 * @enum hydro_var
 * @brief TBD
 */
enum hydro_var { ZFS_IO, H_IO, SURF_IO, PERI_IO, WFS_IO, RH_IO, VEL_IO, Q_IO, NVAR_IO };

/**
 * @enum bio_var
 * @brief bio variables
 * @internal SW 28/05/2018
 */
enum bio_var { PHY_IO, ZOO_IO, BACT_IO, BACTN_IO, BIF_IO, MES_IO, MOP_IO, MOD_IO, SI_D_IO, NH4_IO, NO2_IO, NO3_IO, PO4_IO, O2_IO, N2O_IO, NSPECIES_IO };

/**
 * @enum sed_var
 * @brief TBD
 * @internal SW 23/01/2024 adding sediment layer variables such as volume,height
 */
enum sed_var { SED_VOL_IO, SED_H_IO, NSEDVAR_IO };

/**
 * @enum temp_var
 * @brief TBD
 * @internal NF 11/10/2020 Adding temperature outputs : water (TW) and air (TA)
 */
enum temp_var { TW_IO, TA_IO, NTEMP_IO };

/**
 * @enum modules_io
 * @brief output file types
 */
enum modules_io {
  MASS_IO,       /*!< libaq water mass balance file */
  NSAT_IO,       /*!< libnsat water mass balance file */
  FP_IO,         /*!< libfp water mass balance file */
  HYD_Q_IO,      /*!< libhyd river discharge file */
  HYD_H_IO,      /*!< libhyd river water height and elevation file */
  MBHYD_IO,      /*!< libhyd water mass balance file */
  HDERM_Q_IO,    /*!< libhyd (hyderdermic module) hydraulic network discharge file */
  HDERM_MB_IO,   /*!< libhyd (hyderdermic module) hydraulic network water mass balance file */
  FP_TRA_IO,     /*!< libfp transport mass balance file */
  NSAT_TRA_IO,   /*!< libnsat transport mass balance file */
  HDERM_TRA_IO,  /*!< libhyd (hyderdermic module) transport river "variable of interest (i.e. C, T) file */
  MBHYD_TRA_IO,  /*!< libhyd transport river mass balance file */
  MBAQ_TRA_IO,   /*!< libaq transport mass balance file */
  AQ_H_IO,       /*!< libaq hydraulic head file */
  AQ_THRESH_IO,  /*!< libaq hydraulic head threshold feature file */
  VARAQ_TRA_IO,  /*!< libaq aquifer "variable of interest (i.e. C, T) file */
  VARHYD_TRA_IO, /*!< libhyd surface transport river "variable of interest (i.e. C, T) file */
  MBWET_IO,      /*!< libwet lake mass balance file */
  HWET_IO,       /*!< libwet lake water elevation file */
  NMOD_IO        /*!< Number of output files */
};

/**
 * @enum calculation_unit_type
 * @brief Enumeration used so the user can choose the spatial scale some outputs will be printed at in the command file
 */
enum calculation_unit_type { FP_MTO_CELL_IO, FP_BU_IO, FP_ELE_BU_IO, FP_CPROD_IO, FP_CATCHMENT_IO, FP_SPECIE_UNIT_IO, NSAT_CELL_IO, AQ_CELL_IO, HYD_REACH_IO, HYD_ELE_IO, IO_UNIT_TYPE };

/**
 * @enum time_io
 * @brief TBD
 */
enum time_io { INI_IO, FINAL_IO, CUR_IO, NTIME_IO };

/**
 * @enum type_out
 * @brief TBD
 */
enum type_out { ALL_IO, WIND_IO, NOUT_IO };

/**
 * @enum hyd_flumy
 * @brief TBD
 */
enum hyd_flumy { IZ_IO, IQ_IO, NIFLU };

/**
 * @enum write_type
 * @brief TBD
 */
enum write_type { WRITE_IO, APPEND_IO, NWRITE_TYPE_IO };