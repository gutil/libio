/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libio
 * FILE NAME: struct_io.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libio Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        struct_io.h
 * @brief       IO library structures
 * @author      Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <time.h>

typedef struct file_pile_io s_file_io;
typedef struct input_io s_in_io;
typedef struct simul_io s_simul_io;
typedef struct index_io s_id_io;
typedef struct output_io s_out_io;
typedef struct inout_set s_inout_set_io;

/**
 * @struct file_pile_io
 * @brief Files' attributes
 */
struct file_pile_io {
  /* File's name */
  char *name;
  /* Content of the buffer read ahead */
  void *buffer;
  /* Adress of the file */
  FILE *address;
  /* Currently read line number */
  int line_nb;
};

/**
 * @struct input_io
 * @brief Simulation input reading
 */
struct input_io {
  /* Variables used when reading input data */
  FILE *yyin;
  /* Table of the addresess of the read files */
  s_file_io *current_read_files[NPILE_IO];
  /* Number of folders defined by the user */
  int folder_nb;
  /* File number */
  int pile;
  /* Position within the file being processed */
  int line_nb;
  /* Name of file folders */
  char *name_out_folder[NPILE_IO];
};

/**
 * @struct simul_io
 * @brief Simulation input reading
 */
struct simul_io {
  /* Name of the simulation */
  char *name;
  /* Debug file */
  FILE *poutputs;
  /* Name of the output directory */
  char *name_outputs;
  /* Structure used to parse a file */
  s_in_io *pin;
};

/**
 * @struct output_io
 * @brief Outputs files' attributes
 */
struct output_io {
  /* Stream flux pointer of the file */
  FILE *fout;
  /* File name */
  char *name;
  /*!< Output deltat for each printed variables */
  double deltat;
  /*!< Time in simulation period NTIME in [init,current,output] */
  double t_out[NTIME_IO];
  /* Output time unit */
  double time_unit;
  /* Printed variables & units */
  double hydvar[NVAR_IO];
  /* TBD */
  double hydvar_unit[NVAR_IO];
  /* Printed biological variables & units */ // SW 28/05/2018 coupled with c-rive
  double biovar[NSPECIES_IO];
  /* TBD */
  double biovar_unit[NSPECIES_IO];
  /* Printed sediment layer variables & units */ // SW 23/01/2024
  double biosedvar[NSEDVAR_IO];
  /* TBD */
  double biosedvar_unit[NSEDVAR_IO];
  /* Printed temperature variables & units */ // NF 11/10/2020 coupled with prose-pa
  double tempvar[NTEMP_IO];
  /* TBD */
  double tempvar_unit[NTEMP_IO];
  /*!< List of the elements where output will be printed. */
  s_id_io *id_output;
  /*!< Number of elements printed */
  int nb_output;
  /*!< Type of output in [ALL_IO,WIND_IO,NOUT_IO] */
  int type_out;
  /*!< Format of output in [FORMATED_IO,UNFORMATED_IO,NFORMAT_IO] */
  int format;
  /*!< If final state has to be printed in YES NO*/
  int final_state;
  /*!< If initial state obtained from file in YES NO*/
  int init_from_file;
  /*!< Spatial scale to print out the output file at. Set to CODE_IO by default
   * (ie. left undefined). // NG: 25/07/2020 */
  int spatial_scale;
  /*!< If output file has to be printed out, in [YES,NO].
   * Set to YES by default when initialized */ // NG : 31/07/2020
  int write_out;
};

/**
 * @struct index_io
 * @brief Object index structure
 */
struct index_io {
  /* Object intern ID */
  int id;
  /* In case of an aquifer cell, layer ID */
  int id_lay;
  /* Pointer to the next ID structure */
  s_id_io *next;
  /* Pointer to the previous ID structure */
  s_id_io *prev;
};

/**
 * @struct inout_set
 * @brief TBD
 */
struct inout_set {
  /* Name of the output directory */
  char *name_outputs;
  /* outputs and files for output printing */
  char *name_final_state_file;
  /* TBD */
  int *calc;
  /* Global mass balance file*/ // NF 11/17/06
  FILE *fmb;
  /* TBD */ // 0 for Z and 1 for U
  int init_from_file[2];
};

#define new_file_io() ((s_file_io *)malloc(sizeof(s_file_io)))
#define new_in_io() ((s_in_io *)malloc(sizeof(s_in_io)))
#define new_simul_io() ((s_simul_io *)malloc(sizeof(s_simul_io)))
#define new_id_io() ((s_id_io *)malloc(sizeof(s_id_io)))
#define new_out_io() ((s_out_io *)malloc(sizeof(s_out_io)))
#define new_inout_set() ((s_inout_set_io *)malloc(sizeof(s_inout_set_io)))