/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libio
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libio Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        itos.c
 * @brief       Integer-to-string conversion functions
 * @author      Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include "IO.h"
#include "ext_var_glo_io.h"

/**
 * @brief Name conversion function for output file names
 * @return Output file name as a string
 */
char *IO_name_out(int iname) {
  char *name;
  switch (iname) {

  case MASS_IO: {
    name = strdup("AQ_MB");
    break;
  }
  case NSAT_IO: {
    name = strdup("NONSAT_MB");
    break;
  }
  case FP_IO: {
    name = strdup("WATBAL_MB");
    break;
  }
  case HYD_H_IO: {
    name = strdup("HYD_H");
    break;
  }
  case HYD_Q_IO: {
    name = strdup("HYD_Q");
    break;
  }
  case MBHYD_IO: {
    name = strdup("HYD_MB");
    break;
  }
  case HDERM_Q_IO: {
    name = strdup("HDERM_Q");
    break;
  }
  case HDERM_MB_IO: {
    name = strdup("HDERM_MB");
    break;
  }
  case FP_TRA_IO: {
    name = strdup("WATBAL_TRANSPORT");
    break;
  }
  case NSAT_TRA_IO: {
    name = strdup("NSAT_TRANSPORT");
    break;
  }
  case HDERM_TRA_IO: {
    name = strdup("HDERM_TRANSPORT");
    break;
  }
  case MBHYD_TRA_IO: {
    name = strdup("HYD_MB_TRANSPORT");
    break;
  }
  case MBAQ_TRA_IO: {
    name = strdup("AQ_MB_TRANSPORT");
    break;
  }
  case AQ_H_IO: {
    name = strdup("AQ_H");
    break;
  }
  case AQ_THRESH_IO: {
    name = strdup("AQ_THRESH");
    break;
  }
  case VARAQ_TRA_IO: {
    name = strdup("AQ_VAR_TRANSPORT");
    break;
  }
  case VARHYD_TRA_IO: {
    name = strdup("HYD_VAR_TRANSPORT");
    break;
  }
  case MBWET_IO: {
    name = strdup("MB_WET");
    break;
  }
  case HWET_IO: {
    name = strdup("H_WET");
    break;
  }
  default: {
    name = strdup("Unknown-module");
    break;
  }
  }
  return name;
}

/**
 * @brief Name conversion function for cawaqs library or module
 * @return Library or module name as a string
 */
char *IO_name_mod(int iname) {
  char *name;
  switch (iname) {

  case MASS_IO: {
    name = strdup("AQ");
    break;
  }
  case NSAT_IO: {
    name = strdup("NONSAT");
    break;
  }
  case FP_IO: {
    name = strdup("WATBAL");
    break;
  }
  case HYD_H_IO: {
    name = strdup("HYD");
    break;
  }
  case HYD_Q_IO: {
    name = strdup("HYD");
    break;
  }
  case MBHYD_IO: {
    name = strdup("HYD");
    break;
  }
  case HDERM_Q_IO: {
    name = strdup("HDERM");
    break;
  }
  case HDERM_MB_IO: {
    name = strdup("HDERM");
    break;
  }
  case FP_TRA_IO: {
    name = strdup("WATBAL");
    break;
  }
  case NSAT_TRA_IO: {
    name = strdup("NONSAT");
    break;
  }
  case HDERM_TRA_IO: {
    name = strdup("HDERM");
    break;
  }
  case MBHYD_TRA_IO: {
    name = strdup("HYD");
    break;
  }
  case MBAQ_TRA_IO: {
    name = strdup("AQ");
    break;
  }
  case AQ_H_IO: {
    name = strdup("AQ");
    break;
  }
  case AQ_THRESH_IO: {
    name = strdup("AQ");
    break;
  }
  case VARAQ_TRA_IO: {
    name = strdup("AQ");
    break;
  }
  case VARHYD_TRA_IO: {
    name = strdup("HYD");
    break;
  }
  case MBWET_IO: {
    name = strdup("WET");
    break;
  }
  case HWET_IO: {
    name = strdup("WET");
    break;
  }
  default: {
    name = strdup("Unknown-module");
    break;
  }
  }
  return name;
}

/**
 * @brief Name conversion function for cawaqs calculation object
 * @return Calculation object name as a string
 */
char *IO_geometry_unit_type(int itype) {
  char *name;

  switch (itype) {
  case FP_MTO_CELL_IO: {
    name = strdup("Meteo cell");
    break;
  }
  case FP_BU_IO: {
    name = strdup("Water balance unit");
    break;
  }
  case FP_ELE_BU_IO: {
    name = strdup("Water balance calculation element");
    break;
  }
  case FP_CPROD_IO: {
    name = strdup("Production cell");
    break;
  }
  case FP_CATCHMENT_IO: {
    name = strdup("Catchement");
    break;
  }
  case FP_SPECIE_UNIT_IO: {
    name = strdup("Transport specie unit");
    break;
  }
  case NSAT_CELL_IO: {
    name = strdup("Unsaturated zone cell");
    break;
  }
  case AQ_CELL_IO: {
    name = strdup("Aquifer cell");
    break;
  }
  case HYD_REACH_IO: {
    name = strdup("River reach");
    break;
  }
  case HYD_ELE_IO: {
    name = strdup("River element");
    break;
  }
  default: {
    name = strdup("Unknown CaWaQS geometry unit type.");
    break;
  }
  }
  return name;
}

/**
 * @brief Short name conversion function for cawaqs calculation object
 * @return Calculation object short name as a string
 */
char *IO_geometry_short_name(int itype) {
  char *name;

  switch (itype) {
  case FP_MTO_CELL_IO: {
    name = strdup("MTO");
    break;
  }
  case FP_BU_IO: {
    name = strdup("BU");
    break;
  }
  case FP_ELE_BU_IO: {
    name = strdup("ELE_BU");
    break;
  }
  case FP_CPROD_IO: {
    name = strdup("CPROD");
    break;
  }
  case FP_CATCHMENT_IO: {
    name = strdup("CATCHMENT");
    break;
  }
  case FP_SPECIE_UNIT_IO: {
    name = strdup("SPECIE_UNIT");
    break;
  }
  case NSAT_CELL_IO: {
    name = strdup("NSAT_CELL");
    break;
  }
  case AQ_CELL_IO: {
    name = strdup("AQ_CELL");
    break;
  }
  case HYD_REACH_IO: {
    name = strdup("RIVER_REACH");
    break;
  }
  case HYD_ELE_IO: {
    name = strdup("RIVER_ELEMENT");
    break;
  }
  default: {
    name = strdup("Unknown CaWaQS geometry unit type.");
    break;
  }
  }
  return name;
}

/**
 * @brief Name conversion function for output file format
 * @return Output file format name as a string
 */
char *IO_name_ext(int iext) {
  char *name;
  switch (iext) {
  case FORMATTED_IO:
    name = strdup("Text file");
    break;
  case UNFORMATTED_IO:
    name = strdup("Binary file");
    break;
  default: {
    name = strdup("Not proper format");
    break;
  }
  }
  return name;
}