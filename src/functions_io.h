/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libio
 * FILE NAME: functions_io.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libio Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        functions_io.h
 * @brief       libio functions prototypes
 * @author      Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

// in input.y
int IO_yylex();

// in manage_io.c
s_in_io *IO_create_input();
s_file_io *IO_create_file();
void IO_yyerror(char *, s_in_io *, FILE *);
s_out_io *IO_create_output();
void IO_init_time(s_out_io *, double, double, double);
void IO_write_val(double *, int, FILE *);
void IO_write_header(int *, int, FILE *);
void IO_refresh_t_out(s_out_io *);
s_inout_set_io *IO_create_inout_set(int);
void IO_init_out_file(s_out_io *, int, int, int, FILE *);
void IO_print_bloc_bin(double **, int, int, FILE *);
void *IO_freed(void *);
double **IO_free_val(double **, int);
void IO_link_mb(s_out_io *, int, int, int, FILE *);
s_out_io *IO_init_out_struct(double, double, double);
FILE *IO_return_file(char **, char *, int, FILE *);
void IO_init_final_state_file(s_out_io *, int, FILE *);
void IO_mkdir(const char *);

// in manage_id.c
s_id_io *IO_create_id(int, int);
s_id_io *IO_browse_id(s_id_io *, int);
int IO_length_id(s_id_io *);
s_id_io *IO_chain_fwd_id(s_id_io *, s_id_io *);
s_id_io *IO_chain_bck_id(s_id_io *, s_id_io *);
s_id_io *IO_secured_chain_fwd_id(s_id_io *, s_id_io *);
s_id_io *IO_secured_chain_bck_id(s_id_io *, s_id_io *);
s_id_io *IO_create_id_from_file(char *, FILE *);
s_id_io *IO_free_id(s_id_io *);
s_id_io *IO_free_id_serie(s_id_io *, FILE *);
s_id_io **IO_free_tab_id(s_id_io **, int, FILE *);
void IO_print_id(s_id_io *, FILE *);
s_id_io *IO_copy_id(s_id_io *);
s_id_io *IO_copy_id_serie(s_id_io *, int);
int IO_is_element_id(s_id_io *, s_id_io *);
s_id_io *IO_check_id_list(s_id_io *, s_id_io *, FILE *);

// in itos.c
char *IO_name_mod(int);
char *IO_name_out(int);
char *IO_name_ext(int);
char *IO_geometry_unit_type(int); // NG : 20/07/2020
char *IO_geometry_short_name(int);