#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libio
# FILE NAME: update_version.sh
# 
# CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
# 
# LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
#
# Library developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the libio Library.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/


#Automatic change of the version number
#> update_version.sh <new_version_number>

#gawk -f update_makefile.awk -v nversion=$1 Makefile
#mv awk.out Makefile

gawk -f update_param.awk -v nversion=$1 param_io.h
mv awk.out param_io.h

#gawk -f update_doxygen.awk -v nversion=$1 test_doxygen
#mv awk.out test_doxygen

echo "Version number updated in param_io.h to " $1
