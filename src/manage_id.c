/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libio
 * FILE NAME: manage_id.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of inputs and outputs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libio Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_id.c
 * @brief       Functions dealing with attributes of the s_id_io type object (index pointer)
 * @author      Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "IO.h"
#include "ext_var_glo_io.h"

/**
 * @brief Creates and filles a new index pointer
 * @return Newly created s_id_io* pointer
 */
s_id_io *IO_create_id(int id_lay, int id) {
  s_id_io *pid;

  pid = new_id_io();
  bzero((char *)pid, sizeof(s_id_io));
  pid->id = id;
  pid->id_lay = id_lay;

  return pid;
}

/**
 * @brief Function used to browse a index serie
 * @return Pointer towards the beginning or the end of the serie
 */
s_id_io *IO_browse_id(s_id_io *ptmp, int deb_fin) {
  s_id_io *pid = NULL;
  pid = ptmp;

  if (ptmp != NULL) {
    if (deb_fin == BEGINNING_IO) {
      while (pid->prev != NULL) {
        pid = pid->prev;
      }
    } else {
      while (pid->next != NULL) {
        pid = pid->next;
      }
    }
  }
  return pid;
}

/**
 * @brief Calculates the length of a index serie
 * @return Number of elements in a index serie
 */
int IO_length_id(s_id_io *ptmp) {

  s_id_io *pid;
  int count = 0;

  pid = ptmp;
  if (pid != NULL)
    pid = IO_browse_id(pid, BEGINNING_IO);

  while (pid != NULL) {
    count++;
    pid = pid->next;
  }

  return count;
}

/**
 * @brief Links two elements with structure s_id_io
 */
s_id_io *IO_chain_fwd_id(s_id_io *pid1, s_id_io *pid2) {
  pid1->next = pid2;
  pid2->prev = pid1;

  return pid2;
}

/**
 * @brief Links two elements with structure s_id_io  bck, ie usefull for yacc for undetermined list
 * Read the pile from top (end of the list) to bottom (beginning of the list), but return
 * a pointer to the beginning of the list as a first element
 */
s_id_io *IO_chain_bck_id(s_id_io *pid1, s_id_io *pid2) {
  pid1->next = pid2;
  pid2->prev = pid1;

  return pid1;
}

/**
 * @brief Chains an id with the next one
 */
s_id_io *IO_secured_chain_fwd_id(s_id_io *pid1, s_id_io *pid2) {
  if (pid1 != NULL)
    pid1 = IO_chain_fwd_id(pid1, pid2);
  else
    pid1 = pid2;

  return pid1;
}

/**
 * @brief Chains an id with the next one
 */
s_id_io *IO_secured_chain_bck_id(s_id_io *pid1, s_id_io *pid2) {
  if (pid1 != NULL)
    pid1 = IO_chain_bck_id(pid1, pid2);
  else
    pid1 = pid2;

  return pid1;
}

/**
 * @brief Creates a chain of id index from an input file with two columns(id, id_lay).
 * @return Pointer to the beginning of the chain (first couple of the file being read)
 */
s_id_io *IO_create_id_from_file(char *name, FILE *flog) {

  s_id_io *pid, *pidtmp;
  FILE *fp;
  int eof;
  int id, id_lay;

  pid = NULL;
  pidtmp = NULL;

  fp = fopen(name, "r");
  if (fp == NULL)
    LP_error(flog, "Impossible to open %s\n", name);

  eof = fscanf(fp, "%d %d\n", &id, &id_lay);
  while (eof != EOF) {
    pidtmp = IO_create_id(id_lay, id);
    pid = IO_secured_chain_fwd_id(pid, pidtmp);
    eof = fscanf(fp, "%d %d\n", &id, &id_lay);
  }
  pid = IO_browse_id(pid, BEGINNING_IO);

  return pid;
}

/**
 * @brief Deallocates a single index pointer
 * @return NULL pointer
 */
s_id_io *IO_free_id(s_id_io *pid) {
  free(pid);
  return NULL;
}

/**
 * @brief Deallocates a full index serie
 * @return NULL pointer if success
 */
s_id_io *IO_free_id_serie(s_id_io *pid, FILE *fp) {
  s_id_io *pidtmp;

  pid = IO_browse_id(pid, BEGINNING_IO);

  pidtmp = pid;
  while (pid->next != NULL) {
    pidtmp = pid->next;
    pid = IO_free_id(pid);
    pid = pidtmp;
  }
  pid = IO_free_id(pid);

  if (pid != NULL)
    LP_warning(fp, "libio%4.2f: %s l%d _id pointer not properly deallocated\n", VERSION_IO, __FILE__, __LINE__);

  return pid;
}

/**
 * @brief Deallocates an array of s_id_io* pointers
 * @return NULL pointer if success
 */
s_id_io **IO_free_tab_id(s_id_io **p_id, int nb_id, FILE *fp) {
  int i;

  for (i = 0; i < nb_id; i++) {
    if (p_id[i] != NULL)
      p_id[i] = IO_free_id_serie(p_id[i], fp);
  }
  free(p_id);
  p_id = NULL;

  return p_id;
}

/**
 * @brief Prints the list of indexes within a index chain
 */
void IO_print_id(s_id_io *pid, FILE *fp) {
  s_id_io *pidtmp;
  pidtmp = IO_browse_id(pid, BEGINNING_IO);

  while (pidtmp != NULL) {
    LP_printf(fp, "lay %d id %d\n", pidtmp->id_lay, pidtmp->id);
    pidtmp = pidtmp->next;
  }
}

/**
 * @brief Copies an index
 * @return Copied index pointer
 */
s_id_io *IO_copy_id(s_id_io *pid) {
  s_id_io *pcpy;
  pcpy = IO_create_id(pid->id_lay, pid->id);

  return pcpy;
}

/**
 * @brief Copies an index pointer chain into a new one
 * @return Copied pointer chain
 */
s_id_io *IO_copy_id_serie(s_id_io *pid, int pos) {
  s_id_io *pcpy, *pcpy_tmp, *ptmp;
  pcpy = NULL;
  ptmp = pid;

  if (pos != MIDDLE_IO) {
    ptmp = IO_browse_id(ptmp, BEGINNING_IO);
  }

  while (ptmp != NULL) {
    pcpy_tmp = IO_copy_id(ptmp);

    if (pcpy != NULL) {
      pcpy = IO_secured_chain_fwd_id(pcpy, pcpy_tmp);
    } else
      pcpy = pcpy_tmp;
    ptmp = ptmp->next;
  }
  pcpy = IO_browse_id(pcpy, BEGINNING_IO);

  return pcpy;
}

/**
 * @brief Checks if a s_id_io* pointer belongs to a s_id_io* chain
 * @return YES/NO answer type
 */
int IO_is_element_id(s_id_io *pid1, s_id_io *pid2) {
  int answ = NO_IO;
  s_id_io *ptmp;
  ptmp = pid2;

  while (ptmp != NULL && answ == NO_IO) {
    if (pid1->id == ptmp->id && pid1->id_lay == ptmp->id_lay) {
      answ = YES_IO;
    }
    ptmp = ptmp->next;
  }

  return answ;
}

/**
 * @brief TBD
 * @return TBD
 */
s_id_io *IO_check_id_list(s_id_io *one_id, s_id_io *id_list, FILE *fp) {
  s_id_io *id_tmp;
  int id, id_lay;
  int check_id = CODE_IO, check_lay = CODE_IO;
  id = one_id->id;
  id_lay = one_id->id_lay;

  if (id == id_list->id && id_lay == id_list->id_lay) {
    id_tmp = NULL;
    one_id = IO_free_id_serie(one_id, fp);
  } else {
    id_list = IO_browse_id(id_list, END_IO);
    if (id_list == NULL)
      LP_error(fp, "function io_browse_id returns a null pointer !!! \n");

    while (id_list->prev != NULL) {
      check_id = id_list->id;
      check_lay = id_list->id_lay;
      if (check_id == id && check_lay == id_lay)
        break;
      id_list = id_list->prev;
    }
    if (id_list->prev != NULL) {
      id_tmp = NULL;
      one_id = IO_free_id_serie(one_id, fp);
    } else {
      id_tmp = IO_copy_id_serie(one_id, BEGINNING_IO);
      one_id = IO_free_id_serie(one_id, fp);
    }
    id_list = IO_browse_id(id_list, BEGINNING_IO);
  }

  return id_tmp;
}